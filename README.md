# HSRT Latex Pack

## Install
- install [VS Code](https://code.visualstudio.com/)
- install [MiKTeX](https://miktex.org/) or [TeXlive](https://tug.org/texlive/)
- get gud
## Extensions

### [LaTeX Workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop)
Boost LaTeX typesetting efficiency with preview, compile, autocomplete, colorize, and more.
### [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)
A customizable extension for colorizing matching brackets
### [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
Spelling checker for source code
### [German - Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-german)
German dictionary extension for VS Code
### [GitLens — Git supercharged](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
Supercharge the Git capabilities built into Visual Studio Code — Visualize code authorship at a glance via Git blame annotations and code lens, seamlessly navigate and explore Git repositories, gain valuable insights via powerful comparison commands, and so much more
### [Live Share Extension Pack](https://marketplace.visualstudio.com/items?itemName=ms-vsliveshare.vsliveshare-pack)
Collection of extensions that enable real-time collaborative development with VS Live Share.
### [indent-rainbow](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow)
Makes indentation easier to read
### [Open in new Instance](https://marketplace.visualstudio.com/items?itemName=sydeslyde.open-in-new-instance)
This extension allows you to open a folder or file from the VSCode Explorer in a new Instance of VSCode.
### [Settings Sync](https://marketplace.visualstudio.com/items?itemName=shan.code-settings-sync)
Synchronize Settings, Snippets, Themes, File Icons, Launch, Keybindings, Workspaces and Extensions Across Multiple Machines Using GitHub Gist.

## Useful settings

- "editor.formatOnType": true
- "files.autoSave": "onFocusChange"
- "editor.renderWhitespace": "all"
- "latex-workshop.latex.autoBuild.run": "onSave"
- "cSpell.language": "de,en-GB,en"
- "latex-workshop.chktex.run": "onType"
- "workbench.editor.wrapTabs": true

They can be added by pressing " Ctrl+, " then open the settings.json by clicking the icon on the top right hand corner, add them comma separated.
